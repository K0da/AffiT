/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Properties : Codable {
	let name : String?
	let title : String?
	let municipality : String?
	let municipalityId : Int?
	let type : String?
	let coordName : String?
	let gate : String?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case title = "title"
		case municipality = "municipality"
		case municipalityId = "municipalityId"
		case type = "type"
		case coordName = "coordName"
		case gate = "gate"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		municipality = try values.decodeIfPresent(String.self, forKey: .municipality)
		municipalityId = try values.decodeIfPresent(Int.self, forKey: .municipalityId)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		coordName = try values.decodeIfPresent(String.self, forKey: .coordName)
		gate = try values.decodeIfPresent(String.self, forKey: .gate)
	}

}
