import Foundation

struct WienerLinienAPI {
    let url = URL(string: "http://www.wienerlinien.at/ogd_realtime/monitor?stopId=5")!
    let decoder = MyDecoder()

    func fetchWienerLinien() async throws -> Json4Swift_Base {
        let (data, response) = try await URLSession.shared.data(from: url)
        guard let httpResponse = response as? HTTPURLResponse else {
            throw FetchError.noResponse
        }
        guard httpResponse.statusCode == 200 else {
            throw FetchError.noSuccessfulConnection(errorCode: httpResponse.statusCode)
        }
        return try JSONDecoder().decode(Json4Swift_Base.self, from: data)
    }

    /**
     Takes the decoded structure to read the real time departure time of a station. If there is an error,
     the string contains a human readable error message.
     - Parameter rawResponse: decoded response from the api
     - Returns: String
     */
    func getHumanReadableTime(rawResponse: Json4Swift_Base) -> String {
        guard let depatureTime = rawResponse.data?.monitors?[0].lines?[0].departures?.departure?[0].departureTime?.timeReal else {
            return "No departure time found"
        }
        guard let formatedTime = decoder.decodeTime(time: depatureTime) else {
            return "Error decoding Time"
        }
        return decoder.dateToString(date: formatedTime)
    }

    func getStationName(rawResponse: Json4Swift_Base) -> String {
        guard let stationName = rawResponse.data?.monitors?[0].locationStop?.properties?.title else {
            return "No Name found"
        }
        return stationName
    }
}

