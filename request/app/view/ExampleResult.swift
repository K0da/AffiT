import Foundation
import SwiftUI

struct ExampleResult: View {
    let finder = StationFinder()

    var body: some View {
        VStack {
            Text("Nothing to see")
            Button {
                print(finder.example())
            } label: {
                Text("Button that does nothing")
            }
        }
    }
}

class ExampleResult_Previews: PreviewProvider {
    static var previews: some View {
        ExampleResult()
    }

    #if DEBUG
    @objc class func injected() {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        windowScene?.windows.first?.rootViewController =
                UIHostingController(rootView: ExampleResult())
    }
    #endif
}
