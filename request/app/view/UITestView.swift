import Foundation
import SwiftUI

struct UITestView: View {
    @FocusState private var isFocusedSearchbarOne: Bool
    @FocusState private var isFocusedSearchbarTwo: Bool

    @State private var userInputSearchBarTop = ""
    @State private var userInputSearchBarBot = ""
    @State private var showXButtonOne = false
    @State private var showXButtonTwo = false

    var body: some View {
        VStack {
            VStack {
                SearchBar(
                        text: $userInputSearchBarTop,
                        showDeleteButton: $showXButtonOne
                )
                        .focused($isFocusedSearchbarOne)
                        .onChange(of: isFocusedSearchbarOne) { isFocusedSearchbarOne in
                            if isFocusedSearchbarOne {
                                showXButtonTwo = false
                            }
                        }

                SearchBar(
                        text: $userInputSearchBarBot,
                        showDeleteButton: $showXButtonTwo
                )
                        .focused($isFocusedSearchbarTwo)
                        .onChange(of: isFocusedSearchbarTwo) { isFocusedSearchbarTwo in
                            if isFocusedSearchbarTwo {
                                showXButtonOne = false
                            }
                        }
            }
            Spacer()
            Button("Stop Editing") {
                showXButtonOne = false
                showXButtonTwo = false
                isFocusedSearchbarOne = false
                isFocusedSearchbarTwo = false
            }
        }
    }
}


class UITest_Previews: PreviewProvider {
    static var previews: some View {
        UITestView()
    }

    #if DEBUG
    @objc class func injected() {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        windowScene?.windows.first?.rootViewController =
                UIHostingController(rootView: UITestView())
    }
    #endif
}
