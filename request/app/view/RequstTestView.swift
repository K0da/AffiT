import Foundation
import SwiftUI

struct RequestTestView: View {
    @State private var timeStamp: String = "Next departure time"
    @State private var stationName: String = "Station Name"

    @State private var userInputSearchBarTop = ""
    @State private var showXButton = false

    let request = WienerLinienAPI()

    var body: some View {
        VStack {
            SearchBar(
                    text: $userInputSearchBarTop,
                    showDeleteButton: $showXButton
            )
            Spacer()
            Text(stationName).textSelection(.enabled)
            Text(timeStamp).textSelection(.enabled)
            Button {
                Task {
                    do {
                        let decodedResponse = try await request.fetchWienerLinien()
                        print("Fetched Data successfully!")
                        stationName = request.getStationName(rawResponse: decodedResponse)
                        timeStamp = request.getHumanReadableTime(rawResponse: decodedResponse)
                    } catch {
                        print(error)
                        timeStamp = "Error while requesting data from Wiener Linien"
                        return
                    }
                }
            } label: {
                Text("Fetch Time")
            }
            Spacer()
        }
    }
}

class MainView_Previews: PreviewProvider {
    static var previews: some View {
        RequestTestView()
    }

    #if DEBUG
    @objc class func injected() {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        windowScene?.windows.first?.rootViewController =
                UIHostingController(rootView: UITestView())
    }
    #endif
}
