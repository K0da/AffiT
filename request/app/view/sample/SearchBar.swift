import Foundation
import SwiftUI

struct SearchBar: View {
    @Binding var text: String
    @Binding var showDeleteButton: Bool

    var body: some View {
        HStack {

            TextField("Search", text: $text)
                    .padding(7)
                    .padding(.horizontal, 25)
                    .background(Color(.systemGray6))
                    .cornerRadius(8)
                    .padding(.horizontal, 10)
                    .onTapGesture {
                        self.showDeleteButton = true
                    }
                    .overlay(
                            HStack {
                                Image(systemName: "magnifyingglass")
                                        .foregroundColor(.gray)
                                        .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                                        .padding(.leading, 16)

                                if showDeleteButton {
                                    Button(action: {
                                        self.text = ""
                                    }) {
                                        Image(systemName: "multiply.circle.fill")
                                                .foregroundColor(.gray)
                                                .padding(.trailing, 16)
                                    }
                                }
                            }
                    )
        }
    }
}