import Foundation
import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            RequestTestView()
                    .tabItem {
                        Label("Home", systemImage: "person")
                    }

            UITestView()
                    .tabItem {
                        Label("UITest", systemImage: "book")
                    }

            ExampleResult()
                    .tabItem {
                        Label("CSV", systemImage: "star")
                    }

            Text("Placeholder")
                    .tabItem {
                        Label("Fun Facts", systemImage: "hand.thumbsup")
                    }
        }
    }
}

class ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }

    #if DEBUG
    @objc class func injected() {
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        windowScene?.windows.first?.rootViewController =
                UIHostingController(rootView: ContentView())
    }
    #endif
}
