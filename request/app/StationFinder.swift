import Foundation
import SwiftCSV


struct StationFinder {
    func loadCSV() -> CSV<Named>? {
        do {
            let csvFile: CSV = try CSV<Named>(
                    url: URL(string: "https://www.data.gv.at/katalog/dataset/wiener-linien-echtzeitdaten-via-datendrehscheibe-wien/resource/45f06281-5a9f-441c-9977-0fda610f963a")!,
                    delimiter: .semicolon)
            return csvFile
        } catch CSVParseError.quotation(let info) {
            print("Something went wrong parsing the CSV. Error = \(info)")
            return nil
        } catch {
            print("Something went wrong receiving the CSV. Error: \(error)")
            return nil
        }
    }

    func example() -> String {
        guard let csv: NamedCSV = loadCSV() else {
            return "Nothing found"
        }
        return csv.text
    }
}
