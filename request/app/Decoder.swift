import Foundation

struct MyDecoder {
    // https://nsdateformatter.com/
    // https://stackoverflow.com/questions/35700281/date-format-in-swift

    func decodeTime(time: String) -> Date? {
        let decoder = DateFormatter()
        decoder.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return decoder.date(from: time)
    }

    func dateToString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM. HH:mm"
        return formatter.string(from: date)
    }
}
