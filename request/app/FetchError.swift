import Foundation

enum FetchError: Error {
    case noSuccessfulConnection(errorCode: Int)
    case noResponse
}
