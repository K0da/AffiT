/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Lines : Codable {
	let name : String?
	let towards : String?
	let direction : String?
	let platform : String?
	let richtungsId : String?
	let barrierFree : Bool?
	let realtimeSupported : Bool?
	let trafficjam : Bool?
	let departures : Departures?
	let type : String?
	let lineId : Int?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case towards = "towards"
		case direction = "direction"
		case platform = "platform"
		case richtungsId = "richtungsId"
		case barrierFree = "barrierFree"
		case realtimeSupported = "realtimeSupported"
		case trafficjam = "trafficjam"
		case departures = "departures"
		case type = "type"
		case lineId = "lineId"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		towards = try values.decodeIfPresent(String.self, forKey: .towards)
		direction = try values.decodeIfPresent(String.self, forKey: .direction)
		platform = try values.decodeIfPresent(String.self, forKey: .platform)
		richtungsId = try values.decodeIfPresent(String.self, forKey: .richtungsId)
		barrierFree = try values.decodeIfPresent(Bool.self, forKey: .barrierFree)
		realtimeSupported = try values.decodeIfPresent(Bool.self, forKey: .realtimeSupported)
		trafficjam = try values.decodeIfPresent(Bool.self, forKey: .trafficjam)
		departures = try values.decodeIfPresent(Departures.self, forKey: .departures)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		lineId = try values.decodeIfPresent(Int.self, forKey: .lineId)
	}

}